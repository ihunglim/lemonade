
function compare(a, b) {
	if(a.id==b.id) return 0;
	if (a.id<b.id) return -1;
	if (a.id>b.id) return 1;
}

function printCatList(){	
	$('.cat-list').html('');
	
	$.getJSON("list.json", function(result){
		var cats = result.cats;
		cats.sort(compare);		
		
		var cat_list = "";
		for(i in cats){
			var c = cats[i];
			cat_list += '<div class="cat">'
			+ '<a href="' + c.link + '"><img src="' + c.image + '"></a>'
			+ '<a href="' + c.link + '"><h2>' + c.heading + '</h2></a>'
			+ '<div><p>' + c.content + '</p></div>'
			+ '</div>';
		}
		
		$('.cat-list').html(cat_list);
	});
}
