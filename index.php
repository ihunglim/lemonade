<?
	$json = json_decode(file_get_contents('list.json'));
	$cats = $json->cats;
	
	function sort_cats($a, $b){
		if(($a->id)==($b->id)) return 0;
		return (($a->id)<($b->id))?-1:1;
	}
	usort($cats, 'sort_cats');
	
	$cat_list = "";
	foreach($cats as $c){
		$cat_list .= '<div class="cat">'
		. '<a href="' . $c->link . '"><img src="' . $c->image . '"></a>'
		. '<a href="' . $c->link . '"><h2>' . $c->heading . '</h2></a>'
		. '<div><p>' . $c->content . '</p></div>'
		. '</div>';
	}
?>
<!doctype html>
<html>
<head>
	<title>Lemonade</title>
	<meta name="viewport" content="width=device-width">
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	<!-- <link href="https://fonts.google.com/specimen/Montserrat" rel="stylesheet"> -->
	<link rel="stylesheet" type="text/css" href="style/lemonade.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="script.js"></script>
</head>
<body>
	<div class="header">
		<img class="logo" src="img/logo_lem.png">
	</div>
	<div class="main">
		<div class="banner"><img src="img/banner.jpg"></div>
		<h1>FEATURED</h1>
		<div class="cat-list"><?= $cat_list ?></div>
	</div>
	<div class="footer">
		<img class="logo" src="img/logo_lem.png">
	</div>
	<!--<script> //printCatList(); </script>-->
</body>
</html>